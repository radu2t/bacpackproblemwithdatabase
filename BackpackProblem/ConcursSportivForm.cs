﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackpackProblem
{
    public partial class ConcursSportivForm : Form
    {
        public ConcursSportivForm()
        {
            InitializeComponent();
        }

        private void InsertButton_Click(object sender, EventArgs e)
        {
            
            if (string.IsNullOrEmpty(InputTextBox.Text)||Convert.ToInt32(InputTextBox.Text)<=0||Convert.ToInt32(InputTextBox.Text)>390)
            {
                MessageBox.Show("Please enter Value Between 1 and 390");
            }
            else
            {
            int input=Convert.ToInt32(InputTextBox.Text);
            OutputTextBox.Text = Services.BackPackProblem(input);
            }
            
        }

        private void InputTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;

            if (!Char.IsDigit(character)&& character !=8)
            {
                e.Handled = true;
                ErrorLabel.Text = "Please enter just Digits!";

            }
            else
            {
                ErrorLabel.Text = "You enterd a Digit!";
            }
        }
    }
}
