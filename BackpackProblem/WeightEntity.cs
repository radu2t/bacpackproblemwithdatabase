﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackpackProblem
{
    public class WeightEntity
    {
        public int DiskMass { get; set; }
        public int NumberOfDisks { get; set; }
    }
}
