﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace BackpackProblem
{
    public class Services
    {
        public List<WeightEntity> weightsList = SelectWeightsIntoList();

        public Stack<int> weightStack=new Stack<int>();
        
        public static List<WeightEntity> SelectWeightsIntoList()
        {
            DataAccessDataContext database=new DataAccessDataContext();

            var query = from itemintable in database.Weights orderby itemintable.DiskMass descending select itemintable;

            List<WeightEntity> weightsList=new List<WeightEntity>();

            foreach (var item in query)
            {
                weightsList.Add(new WeightEntity{DiskMass = item.DiskMass,NumberOfDisks = item.NumberOfDisks});
            }

            return weightsList;


        }

        public static string BackPackProblem(int input)
        {
            List<WeightEntity> weightlist = SelectWeightsIntoList();
            List<int>Solution=new List<int>();
            int sum = 0;
            int diference = input;
            int bigestWeight = 0;
            int index = 0;
            StringBuilder output = new StringBuilder();

            if (input<weightlist.Last().DiskMass)
            {
                output.Append("No solution Found");
                return output.ToString();
            }

            while (sum != input)
            {


                if (index>3)
                {
                    break;
                }
                
                    if (diference.Equals(5) || diference.Equals(10))
                    {
                        
                        diference += weightlist[index].DiskMass;
                       Solution.RemoveAt(Solution.Count-1);
                        index++;
                        if (index>3)
                        {
                            break;
                        }
                        bigestWeight = weightlist[index].DiskMass;
                        weightlist[index].NumberOfDisks--;
                    }
                    else if (diference>=0&&diference>=weightlist[index].DiskMass&& weightlist[index].NumberOfDisks>0)
                    
                    {
                        bigestWeight = weightlist[index].DiskMass;
                        weightlist[index].NumberOfDisks--;
                    }
                    else if (diference>=0&&diference<weightlist[index].DiskMass&& weightlist[index].NumberOfDisks>0)
                    {
                        if (index.Equals(3))
                        {
                            break;
                        }
                        index++;
                        bigestWeight = weightlist[index].DiskMass;
                        weightlist[index].NumberOfDisks--;

                    }
                    else
                    {
                         break;
                    }

                if (bigestWeight<=diference)
                {
                    Solution.Add(bigestWeight);
                    diference -= bigestWeight;
                    sum += bigestWeight;
                }
              

                if ((!diference.Equals(5)&&!diference.Equals(10)&&weightlist[index].NumberOfDisks==0))
                {
                    index++;
                }
      
            }

            int SolutionSum = Solution.Sum(x => x);

            if (SolutionSum.Equals(input))
            {

                output.Append("Solution: {");

                foreach (var item in Solution)
                {
                    if (Solution.Last().Equals(item))
                    {
                        output.Append(item+"}");
                    }
                    else
                    {
                        output.Append(item + ", ");
                    }
                }

                output.Append("\r\nTotal Weight Sum: " + SolutionSum + ".\n\n");
                return output.ToString();
            }

           
              output.Append("No solution Found");
                return output.ToString();
           
            
 
        }

      

        
    }
}